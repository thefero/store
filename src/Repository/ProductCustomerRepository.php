<?php

namespace App\Repository;

use App\Entity\ProductCustomer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ProductCustomer|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductCustomer|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductCustomer[]    findAll()
 * @method ProductCustomer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductCustomerRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ProductCustomer::class);
    }

    // /**
    //  * @return ProductCustomer[] Returns an array of ProductCustomer objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductCustomer
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
