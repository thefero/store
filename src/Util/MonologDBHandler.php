<?php

namespace App\Util;

use App\Entity\Log;
use Doctrine\ORM\EntityManagerInterface;
use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Logger;

class MonologDBHandler extends AbstractProcessingHandler
{
    /** @var EntityManagerInterface */
    protected $em;

    public function __construct(EntityManagerInterface $em, int $level = Logger::DEBUG, bool $bubble = true)
    {
        parent::__construct($level, $bubble);
        $this->em = $em;
    }

    /**
     * Writes the record down to the log of the implementing handler
     *
     * @param  array $record
     * @return void
     */
    protected function write(array $record)
    {
        $logEntry = (new Log)
            ->setMessage($record['message'])
            ->setLevel($record['level'])
            ->setLevelName($record['level_name'])
            ->setExtra($record['extra'])
            ->setContext($record['context']);

        $this->em->persist($logEntry);
        $this->em->flush();
    }
}