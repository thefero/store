<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"read"}},
 *     denormalizationContext={"groups"={"write"}}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class Product extends BaseEntity
{
    /**
     * @Assert\Issn
     * @Groups({"read", "write"})
     * @ORM\Column(type="string", length=10)
     */
    private $issn;

    /**
     * @Assert\NotBlank
     * @Groups({"read", "write"})
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @Groups({"read", "write"})
     * @ORM\Column(type="smallint")
     */
    private $status;

    /**
     * @Groups({"read", "write"})
     * @ORM\OneToMany(
     *     targetEntity="App\Entity\ProductCustomer", mappedBy="product", cascade={"persist", "remove"}, orphanRemoval=TRUE
     * )
     */
    private $customers;

    public function __construct()
    {
        $this->customers = new ArrayCollection();
    }

    public function getIssn(): ?string
    {
        return $this->issn;
    }

    public function setIssn(string $issn): self
    {
        $this->issn = $issn;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection|ProductCustomer[]
     */
    public function getCustomers(): Collection
    {
        return $this->customers;
    }

    public function addProductCustomer(ProductCustomer $productCustomer): self
    {
        if (!$this->customers->contains($productCustomer)) {
            $this->customers[] = $productCustomer;
            $productCustomer->setProduct($this);
        }

        return $this;
    }

    public function removeProductCustomer(ProductCustomer $productCustomer): self
    {
        if ($this->customers->contains($productCustomer)) {
            $this->customers->removeElement($productCustomer);
            // set the owning side to null (unless already changed)
            if ($productCustomer->getProduct() === $this) {
                $productCustomer->setProduct(null);
            }
        }

        return $this;
    }
}
