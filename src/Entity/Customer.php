<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"read"}},
 *     denormalizationContext={"groups"={"write"}}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\CustomerRepository")
 */
class Customer extends BaseEntity
{
    public const STATUS_CHOICES = [ 0, 1, 2, 3, 4 ];

    /**
     * @Groups({"read"})
     * @var UuidInterface
     * @ORM\Column(type="uuid", unique=true)
     */
    private $uuid;

    /**
     * @Assert\NotBlank
     * @Groups({"read", "write"})
     * @ORM\Column(type="string", length=150)
     */
    private $firstName;

    /**
     * @Assert\NotBlank
     * @Groups({"read", "write"})
     * @ORM\Column(type="string", length=150)
     */
    private $lastName;

    /**
     * @Assert\DateTime
     * @Groups({"read", "write"})
     * @ORM\Column(type="date", nullable=true)
     */
    private $birthDate;

    /**
     * @Groups({"read", "write"})
     * @Assert\Choice(choices=Customer::STATUS_CHOICES, message = "Choose a valid status.")
     * @ORM\Column(type="smallint")
     */
    private $status;

    /**
     * @ORM\OneToMany(
     *     targetEntity="App\Entity\ProductCustomer", mappedBy="customer", cascade={"persist", "remove"}, orphanRemoval=TRUE
     * )
     */
    private $products;

    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    public function setUuid(string $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getBirthDate(): ?\DateTimeInterface
    {
        return $this->birthDate;
    }

    public function setBirthDate(?\DateTimeInterface $birthDate): self
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getProducts(): Collection
    {
        $products = new ArrayCollection();

        /** @var ProductCustomer $ownedProducts */
        foreach ($this->products as $ownedProducts) {
            $products->add([
                'quantity' => $ownedProducts->getQuantity(),
                'product' => $ownedProducts->getProduct()
            ]);
        }

        return $this->products;
    }

    public function addOwnedProduct(ProductCustomer $ownedProduct): self
    {
        if (!$this->products->contains($ownedProduct)) {
            $this->products[] = $ownedProduct;
            $ownedProduct->setCustomer($this);
        }

        return $this;
    }

    public function removeOwnedProduct(ProductCustomer $ownedProduct): self
    {
        if ($this->products->contains($ownedProduct)) {
            $this->products->removeElement($ownedProduct);
            // set the owning side to null (unless already changed)
            if ($ownedProduct->getCustomer() === $this) {
                $ownedProduct->setCustomer(null);
            }
        }

        return $this;
    }

    /**
     * @ORM\PrePersist
     * @throws \Exception
     */
    public function onPrePersist()
    {
        parent::onPrePersist();
        $this->setUuid(Uuid::uuid4());
    }
}
