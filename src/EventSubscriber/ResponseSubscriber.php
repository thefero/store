<?php

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

final class ResponseSubscriber implements EventSubscriberInterface
{
    /** @var Logger $dbLogger */
    private $dbLogger;

    public function __construct(Logger $dbLogger)
    {
        $this->dbLogger = $dbLogger;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::RESPONSE => [ 'logResponse', EventPriorities::POST_WRITE ],
        ];
    }

    public function logResponse(FilterResponseEvent $event): void
    {
        if (strpos($event->getRequest()->getRequestUri(), '/api/') === 0) {
            $this->dbLogger->debug('Request', $event->getRequest()->headers->all());
            $this->dbLogger->debug('Response', json_decode($event->getResponse()->getContent(), true));
        }
    }
}
