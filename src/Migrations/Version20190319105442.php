<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190319105442 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE customer CHANGE uuid uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\'');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_81398E09D17F50A6 ON customer (uuid)');
        $this->addSql('ALTER TABLE product_customer CHANGE customer_id customer_id INT NOT NULL, CHANGE product_id product_id INT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX UNIQ_81398E09D17F50A6 ON customer');
        $this->addSql('ALTER TABLE customer CHANGE uuid uuid VARCHAR(40) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE product_customer CHANGE customer_id customer_id INT DEFAULT NULL, CHANGE product_id product_id INT DEFAULT NULL');
    }
}
